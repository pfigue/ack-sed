#coding: utf-8

# These parameters are used both, from the command line program
# and from the packaging scripts (i.e. setup.py)

project_name = 'ack-sed'
project_version = '0.1'
project_description = 'Command-line wrapper for the sed command, so you can run it on several files, filtering by file extension.'
