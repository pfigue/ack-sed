#coding: utf-8


import re
from os.path import (isfile, islink, isdir, join, )
try:
    walk
except NameError:
    from os import walk


# Extension list coming from ack-grep, of course.
FILTER_EXTENSION_LIST = [
    ('actionscript', r'.*\.(as|xml)$'),
    ('ada', r'.*\.(ada|adb|ads)$'),
    ('all', r'.*'),
    ('asp', r'.*\.asp$'),
    ('aspx', r'.*\.(master|ascx|asmx|aspx|svc)$'),
    ('batch', r'.*\.(bat|cmd)$'),
    ('cc', r'.*\.(c|h|xs)$'),
    ('cfmx', r'.*\.(cfc|cfm|cfml)$'),
    ('clojure', r'.*\.clj$'),
    ('cmake', r'^(CMakeLists.txt|.*\.cmake)$'),
    ('coffeescript', r'.*\.coffee$'),
    ('cpp', r'.*\.(cpp|cc|cxx|m|hpp|hh|h|hxx)$'),
    ('csharp', r'.*\.cs$'),
    ('css', r'.*\.css$'),
    ('dart', r'.*\.dart$'),
    ('deplhi', r'.*\.(pas|int|dfm|nfm|dof|dpk|dproj|groupproj|bdsgroup|bdsproj)$'),
    ('elisp',r'.*\.el$'),
    ('elixir', r'.*\.(ex|exs)$'),
    ('erlang', r'.*\.(erl|hrl)$'),
    ('fortran', r'.*\.(f|f77|f90|f95|f03|for|ftn|fpp)$'),
    ('go', r'.*\.go$'),
    ('groovy', r'.*\.(groovy|gtmpl|gpp|grunit|gradle)$'),
    ('haskell', r'.*\.(hs|lhs)$'),
    ('hh',r'.*\.h$'),
    ('html', r'.*\.(htm|html)$'),
    ('java', r'.*\.(java|properties)$'),
    ('js',r'.*\.js$'),
    ('json',r'.*\.json$'),
    ('jsp', r'.*\.(jsp|jspx|jhtm|jhtml)$'),
    ('less',r'.*\.less$'),
    ('lisp', r'.*\.(lisp|lsp)$'),
    ('lua',r'.*\.lua$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('make', r'^(makefile|Makefile|GNUmakefile|.*\.mak|.*\.mk)$'),
    ('matlab',r'.*\.m$'),
    ('objc', r'.*\.(m|h)$'),
    ('objcpp', r'.*\.(mm|h)$'),
    ('ocaml', r'.*\.(ml|mli)$'),
    ('parrot', r'.*\.(pir|pasm|pmc|ops|pod|pg|tg)$'),
    ('perl', r'.*\.(pl|pm|pod|t|psgi)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('perltest',r'.*\.t$'),
    ('php', r'.*\.(php|phpt|php3|php4|php5|phtml)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('plone', r'.*\.(pt|cpt|metadata|cpy|py)$'),
    ('python', r'.*\.py$'),  ## FIXME ack-grep checks also the 1st line of the file.
    ('rake', r'^Rakefile$'),
    ('rr',r'.*\.R$'),
    ('ruby', r'^(Rakefile|.*\.rb|.*\.rhtml|.*\.rjs|.*\.rxml|.*\.erb|.*\.rake|.*\.spec)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('rust',r'.*\.rs$'),
    ('sass', r'.*\.(sass|scss)$'),
    ('scala',r'.*\.scala$'),
    ('scheme', r'.*\.(scm|ss)$'),
    ('shell', r'.*\.(sh|bash|csh|tcsh|ksh|zsh|fish)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('smalltalk',r'.*\.st$'),
    ('sql', r'.*\.(sql|ctl)$'),
    ('tcl', r'.*\.(tcl|itcl|itk)$'),
    ('tex', r'.*\.(tex|cls|sty)$'),
    ('tt', r'.*\.(tt|tt2|ttml)$'),
    ('vb', r'.*\.(bas|cls|frm|ctl|vb|resx)$'),
    ('verilog', r'.*\.(v|vh|sv)$'),
    ('vhdl', r'.*\.(vhd|vhdl)$'),
    ('vim',r'.*\.vim$'),
    ('xml', r'.*\.(xml|dtd|xsl|xslt|ent)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('yaml', r'.*\.(yml|yaml)$'),
]


class SourceFileGenerator(object):

    @classmethod
    def get_file_argument_as_list(self, arguments):
        temp = arguments['<file>']
        if type(temp) is list:
            if len(temp) == 0:
                return ['./',]
            else:
                return temp
        else:
            return [temp,]

    @classmethod
    def get_extension_arguments_as_list(self, arguments):
        extension_inclusion_list = []
        extension_exclusion_list = []

        arguments['--noall'] = False # NOTE: dirty hack

        #FIXME remove this traces
        #print arguments
        get_extension_if_filter_in =  lambda dictionary:\
            (lambda (keyword, extension):
                extension if dictionary.get('--%s' % keyword, None) == True else None)
        get_noextension_if_filter_in =  lambda dictionary:\
            (lambda (keyword, noextension):
                noextension if dictionary.get('--no%s' % keyword, None) == True else None)

        extension_inclusion_list = filter(lambda x: x is not None,
            map (get_extension_if_filter_in(arguments), FILTER_EXTENSION_LIST))
        extension_exclusion_list = filter(lambda x: x is not None,
            map (get_noextension_if_filter_in(arguments), FILTER_EXTENSION_LIST))

        #FIXME remove this traces
        #print 'extension_inclusion_list: %s' % extension_inclusion_list
        #print 'extension_exclusion_list: %s' % extension_exclusion_list
        return (extension_inclusion_list, extension_exclusion_list)

    @classmethod
    def filter_files(self, file_list, extension_inclusion_list, extension_exclusion_list):
        # FIXME develop the functional version :)

        #decide_file_on_one_extension = lambda filename, extension: filename if re.match(filename, extension) else None
        #decide_file_on_extension_list = lambda filename, extension_list: map(decide_file_on_one_extension, filename, extension_list)
        #apply_inclusion_list = lambda filename: map(decide_file_on_extension_list, filename, extension_inclusion_list)

        #apply_inclusion_list = lambda filename: map (
        #    lambda expression: filename if re.match(filename, expression) else None,
        #    extension_inclusion_list)
        #apply_exclusion_list = lambda filename: map (
        #    lambda expression: filename if not re.match(filename, expression) else None,
        #    extension_exclusion_list)

        #file_list = filter(lambda x: x is not None, map(apply_inclusion_list, file_list))
        #file_list = filter(lambda x: x is not None, map(apply_exclusion_list, file_list))

        # FIXME REs are not compiled

        #print "extension_inclusion_list: %s" % extension_inclusion_list
        temporary_result = []
        for file_name in file_list:
            for extension in extension_inclusion_list:
                #print "re.match(extension, file_name) := re.match('%s', '%s') := %s" % (extension, file_name, re.match(extension, file_name))
                if re.match(extension, file_name):
                    temporary_result.append(file_name)
                    break
        file_list = temporary_result
        #print "temporary_result: %s" % temporary_result

        #print "extension_exclusion_list: %s" % extension_exclusion_list
        temporary_result = []
        for file_name in file_list:
            for extension in extension_exclusion_list:
                #print "re.match(extension, file_name) := re.match('%s', '%s') := %s" % (extension, file_name, re.match(extension, file_name))
                if re.match(extension, file_name):
                    temporary_result.append(file_name)
                    break
        #print "temporary_result: %s" % temporary_result
        
        for file_name in temporary_result:
            file_list.remove(file_name)
        #FIXME do a trace("") call: if env. var TRACE=="PRINT" just print, if it is "CRASH", then raise an exception, in any other case, just ignore
        return file_list

    def __init__(self, cmdline_arguments):
        self.path_list = self.get_file_argument_as_list(cmdline_arguments)
        (self.extension_inclusion_list,
         self.extension_exclusion_list) = self.get_extension_arguments_as_list(cmdline_arguments)

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        # With an invocation like
        #   ack-sed "s/foo/bar/g" house/ house/kitchen/
        # The files under house/kitchen/ may be processed more than once, which is suboptimal.
        # NOTE: sed'ing a file several times can be a bug for some SEd expressions? 
        # Solutions:
        #   + Stop using generators and return a (huge) list w/ all the files
        #   + Keep a list in the generator with the files already returned. Check against the list before returning a file. Time complexity grows. Space consumption also.
        #   + Forget about it: the user rarely is going to provide the same dir. several times, and actually sed the same file several times won't be too bad, unless we sed in parallel
        #   NOTE: we don't follow symlinks. Following them might cause infinite loops in this algortihm
        while self.path_list:
            print 'path_list = %s' % self.path_list
            path = self.path_list.pop(0)
            #print 'next():path = %s' % path #FIXME remove traces
            if isfile(path) and not islink(path):
                return path
            elif isdir(path) and not islink(path):
                walker_obj = walk(path)
                (root, dir, files) = walker_obj.next()
                self.path_list.extend(
                        [join(root, d) for d in dir] + #FIXME first files, then dirs., to avoid ahaving abig list. Note that "walking" can create layers: [files, dirs, files, dirs, etc.] while it should be better something like [files, dirs]
                        [join(root, f) for f in self.filter_files(files,
                            self.extension_inclusion_list,
                            self.extension_exclusion_list)])
                    #print 'root, dir, files = %s, %s, %s' % (root, dir, files)
        raise StopIteration()
    #FIXME the testing for this can be a) unittesting + mock, b) functional testing w/ a backpack
