ack-sed
=======

Command line wrapper for the sed utility, a la ack-grep

You can launch a `sed` instance against several files, for example, all the \*.py files under a directory.

Install
-------
::

	pip install ack-sed

Usage
-----
::

	ack-sed [options] <path>

For example:
::

	ack-sed --python ./
	ack-sed --yaml /tmp/config/


Install for development
-----------------------
You will need: *git*, *python2.7*, *virtualenv-wrapper* properly installed and configured, and *pip*.
::

	$ git clone https://bitbucket.org/pfigue/ack-sed.git
	[...]
	$ cd ack-sed/
	$ mkvirtualenv -p /usr/bin/python2.7 ack-sed
	[...]
	$ workon ack-sed
	(ack-sed) $ pip install -r requirements.txt
	(ack-sed) $

Running the tests is something you can do just with `nosetests`::

	(ack-sed) $ nosetests
	......................
	----------------------------------------------------------------------
	Ran 22 tests in 0.262s

	OK
	(ack-sed) $
