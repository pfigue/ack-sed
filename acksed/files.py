#coding: utf-8


import re
from os import walk
from os.path import join


# Extension list coming from ack-grep, of course.
FILTER_EXTENSION_LIST = [
    ('actionscript', r'.*\.(as|xml)$'),
    ('ada', r'.*\.(ada|adb|ads)$'),
    ('all', r'.*'),
    ('asp', r'.*\.asp$'),
    ('aspx', r'.*\.(master|ascx|asmx|aspx|svc)$'),
    ('batch', r'.*\.(bat|cmd)$'),
    ('cc', r'.*\.(c|h|xs)$'),
    ('cfmx', r'.*\.(cfc|cfm|cfml)$'),
    ('clojure', r'.*\.clj$'),
    ('cmake', r'^(CMakeLists.txt|.*\.cmake)$'),
    ('coffeescript', r'.*\.coffee$'),
    ('cpp', r'.*\.(cpp|cc|cxx|m|hpp|hh|h|hxx)$'),
    ('csharp', r'.*\.cs$'),
    ('css', r'.*\.css$'),
    ('dart', r'.*\.dart$'),
    ('deplhi', r'.*\.(pas|int|dfm|nfm|dof|dpk|dproj|groupproj|bdsgroup|bdsproj)$'),
    ('elisp',r'.*\.el$'),
    ('elixir', r'.*\.(ex|exs)$'),
    ('erlang', r'.*\.(erl|hrl)$'),
    ('fortran', r'.*\.(f|f77|f90|f95|f03|for|ftn|fpp)$'),
    ('go', r'.*\.go$'),
    ('groovy', r'.*\.(groovy|gtmpl|gpp|grunit|gradle)$'),
    ('haskell', r'.*\.(hs|lhs)$'),
    ('hh',r'.*\.h$'),
    ('html', r'.*\.(htm|html)$'),
    ('java', r'.*\.(java|properties)$'),
    ('js',r'.*\.js$'),
    ('json',r'.*\.json$'),
    ('jsp', r'.*\.(jsp|jspx|jhtm|jhtml)$'),
    ('less',r'.*\.less$'),
    ('lisp', r'.*\.(lisp|lsp)$'),
    ('lua',r'.*\.lua$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('make', r'^(makefile|Makefile|GNUmakefile|.*\.mak|.*\.mk)$'),
    ('matlab',r'.*\.m$'),
    ('objc', r'.*\.(m|h)$'),
    ('objcpp', r'.*\.(mm|h)$'),
    ('ocaml', r'.*\.(ml|mli)$'),
    ('parrot', r'.*\.(pir|pasm|pmc|ops|pod|pg|tg)$'),
    ('perl', r'.*\.(pl|pm|pod|t|psgi)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('perltest',r'.*\.t$'),
    ('php', r'.*\.(php|phpt|php3|php4|php5|phtml)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('plone', r'.*\.(pt|cpt|metadata|cpy|py)$'),
    ('python', r'.*\.py$'),  ## FIXME ack-grep checks also the 1st line of the file.
    ('rake', r'^Rakefile$'),
    ('rr',r'.*\.R$'),
    ('ruby', r'^(Rakefile|.*\.rb|.*\.rhtml|.*\.rjs|.*\.rxml|.*\.erb|.*\.rake|.*\.spec)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('rust',r'.*\.rs$'),
    ('sass', r'.*\.(sass|scss)$'),
    ('scala',r'.*\.scala$'),
    ('scheme', r'.*\.(scm|ss)$'),
    ('shell', r'.*\.(sh|bash|csh|tcsh|ksh|zsh|fish)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('smalltalk',r'.*\.st$'),
    ('sql', r'.*\.(sql|ctl)$'),
    ('tcl', r'.*\.(tcl|itcl|itk)$'),
    ('tex', r'.*\.(tex|cls|sty)$'),
    ('tt', r'.*\.(tt|tt2|ttml)$'),
    ('vb', r'.*\.(bas|cls|frm|ctl|vb|resx)$'),
    ('verilog', r'.*\.(v|vh|sv)$'),
    ('vhdl', r'.*\.(vhd|vhdl)$'),
    ('vim',r'.*\.vim$'),
    ('xml', r'.*\.(xml|dtd|xsl|xslt|ent)$'), ## FIXME ack-grep checks also the 1st line of the file.
    ('yaml', r'.*\.(yml|yaml)$'),
]


def get_file_argument_as_list(arguments):
    temp = arguments['<file>']
    if type(temp) is list:
        if len(temp) == 0:
            return ['./',]
        else:
            return temp
    else:
        return [temp,]


def prepare_include_and_exclude_lists(arguments):
	include = []
	exclude = []
	for (extension, exp) in FILTER_EXTENSION_LIST:
		if arguments.get('--%s' % extension, False) is not False:
			include.append(re.compile(exp))
		elif arguments.get('--no%s' % extension, False) is not False:
			exclude.append(re.compile(exp))

	if not include:
		include = [re.compile(r'.*$'),]

	return (include, exclude)


def apply_filters_to_file(filename, include, exclude):
	for regexp in exclude:
		#regexp = re.compile(exp)  # FIXME too slow
		if regexp.match(filename):
			have_to_include = False
			have_to_exclude = True
			return (have_to_include, have_to_exclude)

	for regexp in include:
		#regexp = re.compile(exp)  # FIXME too slow
		if regexp.match(filename):
			have_to_include = True
			have_to_exclude = False
			return (have_to_include, have_to_exclude)

	have_to_include = False
	have_to_exclude = False
	return (have_to_include, have_to_exclude)


def walk_and_filter_file_paths(arguments):
	path_list = get_file_argument_as_list(arguments)
#	print path_list

	(include, exclude) = prepare_include_and_exclude_lists(arguments)
#	print include
#	print exclude

	file_list = []

	while path_list:
		path = path_list.pop(0)
		# print path
		for root, directories, files in walk(path):
			# print root, directories, files
			# here we can modify directories, with filtering
			for this_file in files:
				# exclude?
				(have_to_include, have_to_exclude) = apply_filters_to_file(this_file, include, exclude)
				# print (this_file, have_to_include, have_to_exclude)
				if have_to_include:
					this_file_abspath = join(root, this_file)
					if not this_file_abspath in file_list:
						file_list.append(this_file_abspath)

	return file_list
