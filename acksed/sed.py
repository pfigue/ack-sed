#coding: utf-8

import shutil
from os import unlink
from tempfile import NamedTemporaryFile
from subprocess import call


def sed_one_file(expression, file_name):
    temp_file_fd = NamedTemporaryFile(mode='w',
                    suffix='.tmp', prefix='ack_sed_',
                    delete=False)
    temp_file_name = temp_file_fd.name
    temp_file_fd.close()
    try:
# NOTE: here comes a shell command injection
        call('/bin/sed "%s" %s > %s' % (expression, file_name, temp_file_name), shell=True)
    except Exception as k:
        raise k
    unlink(file_name)
    shutil.move(temp_file_name, file_name)
