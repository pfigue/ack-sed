#!/usr/bin env python
#coding: utf-8


from nose.tools import (istest, )
from shutil import copytree, rmtree
from main import SourceFileGenerator
from os.path import (join, dirname, )
from os import (chdir, getcwd, )

#def backpack(path):
    #base_dir = '/home/pablo/Workspace/ack-sed/tests/func/'
    #base_dir = './../'
#    return path #join(base_dir, 'backpack', path)

@istest
class SourceFileGenerator_JustFiles_Tests(object):

    def setUp(self):
        print 'setup_func ...'
        print "__name__: %s" % __file__
        self.old_dir = getcwd()
        self.base_dir = join(dirname(__file__), 'backpack/')
        backpack_path = join(dirname(__file__), self.__class__.__name__)
        print "base_dir: %s" % self.base_dir
        print "backpack_path: %s" % backpack_path
        #base_dir = '/home/pablo/Workspace/ack-sed/tests/func/'
#idea: take dir() from __name__, append backpack/ and with it build the base_dir, cd there, and all the paths should be relative to that point
        rmtree(self.base_dir, ignore_errors=True)
        copytree(backpack_path, self.base_dir, symlinks=False)
        chdir(self.base_dir)
        print 'setup_func finished'

    def tearDown(self):
        chdir(self.old_dir)
        rmtree(self.base_dir, ignore_errors=True)

    #@istest
    def test_files(self):
        print 'test_files...'
        my_gen = SourceFileGenerator({'<file>': ['file1', 'file2', 'file3', ], })
        print my_gen
        source_file_list = [f for f in my_gen]
        print source_file_list
        source_file_list.sort()
        print source_file_list
        assert source_file_list == ['file1', 'file2', 'file3', ]


@istest
class SourceFileGenerator_WithSubdir_Tests(object):

    def setUp(self):
        print 'setup_func ...'
        print "__name__: %s" % __name__
        print "self.__class__.__name__: %s" % self.__class__.__name__
        print "__file__: %s" % __file__
        self.old_dir = getcwd()
        self.base_dir = join(dirname(__file__), 'backpack/')
        backpack_path = join(dirname(__file__), self.__class__.__name__)
        print "base_dir: %s" % self.base_dir
        print "backpack_path: %s" % backpack_path
        rmtree(self.base_dir, ignore_errors=True)
        copytree(backpack_path, self.base_dir, symlinks=False)
        chdir(self.base_dir)
        print 'setup_func finished'

    def tearDown(self):
        chdir(self.old_dir)
        rmtree(self.base_dir, ignore_errors=True)

    def test_files(self):
        print 'test_files...'
        my_gen = SourceFileGenerator({'<file>': ['file1', 'file2', 'dir1/', ], })
        print my_gen
        source_file_list = [f for f in my_gen]
        print source_file_list
        source_file_list.sort()
        print source_file_list
        assert source_file_list == ['dir1/file3', 'file1', 'file2', ]
