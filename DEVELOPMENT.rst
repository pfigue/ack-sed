# Development of ack-sed

## Installing ack-sed for development

### Requirements to install ack-sed for development

You will need: *git*, *python2.7*, *virtualenv-wrapper* properly installed and *pip*.


### Steps to install ack-sed for development

The steps are: clone the repository, create the Python Virtual Environment and populate it with the software requirements.

	user@macchinetta:~/ $ git clone git@github.com:pfigue/ack-sed.git
	user@macchinetta:~/ $ cd ack-sed/
	user@macchinetta:~/ack-sed/ $ mkvirtualenv --python2.7 ack-sed
	user@macchinetta:~/ack-sed/ $ workon ack-sed
	(ack-sed) user@macchinetta:~/ack-sed/ $ pip install -r requirements.txt


## Running integration and unitary tests

Both, integration and unitary tests are written and intended to be run with the *nosetests* suite.

So if you want to run the unitary tests for *SourceFileGenerator*, you can just run, from the virtualenv and from the root directory of the project:

	(ack-sed) user@macchinetta:~/ack-sed/ $ nosetests tests/unit/SourceFileGeneratorTests.py


## Development workflow

FIXME describe here how to commit changes, create branches, PRs, etc.