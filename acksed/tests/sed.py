#!/usr/bin env python
#coding: utf-8


import hashlib
from unittest import TestCase
from shutil import copytree, rmtree
from acksed import sed_one_file
from os.path import (join, dirname, )
from os import (chdir, getcwd, )


# FIXME al this boiler plate to a different project
def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def md5sum(filename):
    with open(filename, 'rb') as f:
        result = hashfile(f,hashlib.md5())
    return result


class sed_one_fileTest(TestCase):

    def setUp(self):
        print 'setup_func ...'
        print "__name__: %s" % __name__
        print "self.__class__.__name__: %s" % self.__class__.__name__
        print "__file__: %s" % __file__
        self.old_dir = getcwd()
        self.base_dir = join(dirname(__file__), 'backpack/')
        backpack_path = join(dirname(__file__), self.__class__.__name__)
        print "base_dir (dest): %s" % self.base_dir
        print "backpack_path (origin): %s" % backpack_path
        rmtree(self.base_dir, ignore_errors=True)
        copytree(backpack_path, self.base_dir, symlinks=False)
        chdir(self.base_dir)
        print 'setup_func finished'

    def tearDown(self):
        chdir(self.old_dir)
        rmtree(self.base_dir, ignore_errors=True)

    def test_if_my_test_file_arrived_properly(self):
        actual_hash = md5sum('lorem_ipsum.txt')
        print 'actual_hash:', actual_hash
        self.assertEqual(actual_hash, 'c30d856f939dbae76309386a5ee481f7')

    def test_if_i_can_replace_et_with_ET(self):
        sed_one_file(r's/et/ET/g', 'lorem_ipsum.txt')
        actual_hash = md5sum('lorem_ipsum.txt')
        print 'actual_hash:', actual_hash
        self.assertEqual(actual_hash, 'dd21d2ef27cf69e07ea38f27b80763f2')
