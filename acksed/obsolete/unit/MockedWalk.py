#!/usr/bin env python
#coding: utf-8

from os import walk
from nose.tools import (istest, raises, )


class MockedWalkGenerator(object):
    def __init__(self, path):
        self.path = path

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self):
        if self.path == '':
            raise StopIteration

        # result = {
        #     './':('./',
        #         ['dir1', 'dir2', ],
        #         ['file3', ]),
        #     'dir1':('./dir1',
        #         [],
        #         ['file1', ]),
        #     'dir2':('./dir2',
        #         [],
        #         ['file2', ]),
        # }[self.path]

        result = {
            './':('./',
                ['dir1', 'dir2', ],
                ['file3', ]),
            './dir1':('./dir1',
                [],
                ['file1', ]),
            './dir2':('./dir2',
                [],
                ['file2', ]),
        }[self.path]
        next_path = {
            './':'./dir1',
            './dir1':'./dir2',
            './dir2':'',
        }[self.path]

        self.path = next_path
        return result


@istest
class MockedWalkTest(object):
    @staticmethod
    def mocked_walk(path):
        iterator = MockedWalkGenerator(path)
        return iterator

    def setUp(self):
        global walk

        self.old_walk = walk
        walk = self.mocked_walk

    def tearDown(self):
        global walk
        walk = self.old_walk

    @raises(StopIteration)
    def test_mocked_walk_EmptyPath(self):
        generator = walk('')
        generator.next()

    def test_mocked_walk_1st(self):
        generator = walk('./')
        assert generator.next() == ('./', ['dir1', 'dir2'], ['file3',])
        #assert walk('') == ('', ['dir1', 'dir2'], ['file3',])

    def test_mocked_walk_2nd(self):
        generator = walk('./')
        generator.next()
        result = generator.next()
        print 'result = %s' % str(result)
        assert result == ('./dir1', [], ['file1'])

    def test_mocked_walk_3rd(self):
        generator = walk('./')
        generator.next()
        generator.next()
        result = generator.next()
        print 'result = %s' % str(result)
        assert result == ('./dir2', [], ['file2',])

    #FIXME make a test when the generator raises a StopIteration
    @raises(StopIteration)
    def test_mocked_walk_4th(self):
        generator = walk('./')
        generator.next()
        generator.next()
        generator.next()
        generator.next()
