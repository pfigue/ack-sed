#!/usr/bin env python
#coding: utf-8


from nose.tools import (istest, raises, )
from SourceFileGenerator import SourceFileGenerator

#
# get_file_argument_as_list
#

@istest
def get_file_argument_as_list_empty():
    arguments = {
            '<file>': [],
    }
    result = SourceFileGenerator.get_file_argument_as_list(arguments)
    print 'result = %s' % result
    assert result == ['./',]


@istest
def get_file_argument_as_list_LocalDir():
    arguments = {
            '<file>': ['./'],
    }
    result = SourceFileGenerator.get_file_argument_as_list(arguments)
    print 'result = %s' % result
    assert result == ['./',]


@istest
def get_file_argument_as_list_TwoDirs():
    arguments = {
            '<file>': ['foo/', 'bar/'],
    }
    result = SourceFileGenerator.get_file_argument_as_list(arguments)
    print 'result = %s' % result
    assert result == ['foo/', 'bar/']


#
# get_extension_arguments_as_list
#

@istest
def get_extension_arguments_as_list_empty():
    arguments = {
        '--all': False,
        '--nopython': False,
        '--python': False,
    }
    result = SourceFileGenerator.get_extension_arguments_as_list(arguments)
    print 'result = %s' % str(result)
    assert result == ([], [])
    # FIXME per default it should take all files, if no --flags are specified.


@istest
def get_extension_arguments_as_list_OnlyPython():
    arguments = {
        '--all': False,
        '--nopython': False,
        '--python': True,
    }
    result = SourceFileGenerator.get_extension_arguments_as_list(arguments)
    print 'result = %s' % str(result)
    assert result == (['.*\\.py$'], [])


@istest
def get_extension_arguments_as_list_AllFiles():
    arguments = {
        '--all': True,
        '--nopython': False,
        '--python': False,
    }
    result = SourceFileGenerator.get_extension_arguments_as_list(arguments)
    print 'result = %s' % str(result)
    assert result == (['.*'], [])


@istest
def get_extension_arguments_as_list_AllFilesAndPython():
    arguments = {
        '--all': True,
        '--nopython': False,
        '--python': True,
    }
    result = SourceFileGenerator.get_extension_arguments_as_list(arguments)
    print 'result = %s' % str(result)
    assert result == (['.*', '.*\\.py$'], [])


@istest
def get_extension_arguments_as_list_PythonAndNoPython():
    arguments = {
        '--all': False,
        '--nopython': True,
        '--python': True,
    }
    result = SourceFileGenerator.get_extension_arguments_as_list(arguments)
    print 'result = %s' % str(result)
    assert result == (['.*\\.py$'], ['.*\\.py$'])


#
# filter_files
#
@istest
def filter_files_AllFiles():
    result = SourceFileGenerator.filter_files(file_list=['foobar.py', 'README.rst', 'main.py'],
                                    extension_inclusion_list=['.*'],
                                    extension_exclusion_list=[])
    print 'result = %s' % str(result)
    assert result == ['foobar.py', 'README.rst', 'main.py']


@istest
def filter_files_PythonFiles():
    result = SourceFileGenerator.filter_files(file_list=['foobar.py', 'README.rst', 'main.py'],
                                    extension_inclusion_list=['.*\\.py$'],
                                    extension_exclusion_list=[])
    print 'result = %s' % str(result)
    assert result == ['foobar.py', 'main.py']


@istest
def filter_files_PythonAndNoPythonFiles():
    result = SourceFileGenerator.filter_files(file_list=['foobar.py', 'README.rst', 'main.py'],
                                    extension_inclusion_list=['.*\\.py$'],
                                    extension_exclusion_list=['.*\\.py$'])
    print 'result = %s' % str(result)
    assert result == []


@istest
def filter_files_EmptyDir():
    result = SourceFileGenerator.filter_files(file_list=[],
                                    extension_inclusion_list=['.*\\.py$'],
                                    extension_exclusion_list=['.*\\.py$'])
    print 'result = %s' % str(result)
    assert result == []


#
# SourceFileGenerator iteration habilities
#

from MockedOsPathFunctions import MockedOsPathFunctions
from MockedWalk import MockedWalkTest

from os import walk
from os.path import (isfile, isdir, islink)

@istest
class SourceFileGeneratorTests(object):

    def setUp(self):
        global isfile, isdir, islink, walk, SourceFileGenerator
        
        self.old_isfile = isfile
        self.old_isdir = isdir
        self.old_islink = islink
        self.old_walk = walk

        isfile = MockedOsPathFunctions.isfile
        isdir = MockedOsPathFunctions.isdir
        islink = MockedOsPathFunctions.islink
        walk = MockedWalkTest.mocked_walk

        del(SourceFileGenerator)
        from main import SourceFileGenerator

    def tearDown(self):
        global isfile, isdir, islink, walk
        
        isfile = self.old_isfile
        isdir = self.old_isdir
        islink = self.old_islink
        walk = self.old_walk

    def test_1_FIXME(self):
        arguments = {
            '--all': True,
            '--help': False,
            '--nopython': False,
            '--python': False,
            '--version': False,
            '<file>': ['./'],
            '<sed-expression>': 's/ABC/DEF/g'}
        obj = SourceFileGenerator(arguments)
        one_file = obj.next()
        print "one_file: %s" % one_file
        one_file = obj.next()
        print "one_file: %s" % one_file
        one_file = obj.next()
        print "one_file: %s" % one_file

