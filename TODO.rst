# TO DO

## Work in Progress

	- Create a debug_helpers module with trace("") and return(""), checking for env. var. Would like to print just from given modules: like a logger where you can specify a diff. log level for each module
	- sed_one_fileTest boilerplate
	- automated test complete command
	- Check everything all together is working
	- Remove FIXMES and comments
	- Complete README.md and DEVELOPMENT.md
	- Prepare package

## Bugs to solve

	- Look for FIXME in the code. `ack-grep --python FIXME`


## Needed features

## Nice-to-have features

	- Omit binary files
	- --ignore-file/--ignore-dir options
	- Print files processed, or files where sed returned 0
	- Make nosetests run with just `nosetests`.
	- Test at least some of the extensions --clojure, --cmake, --batch
