#!/usr/bin env python
#coding: utf-8


from nose.tools import (istest, raises, )
from os.path import (isfile, islink, isdir)

@istest
class MockedOsPathFunctions(object):
    @staticmethod
    def isfile(path):
        return {
            './': False,
            './dir1':False,
            './dir2':False,
            './dir1/file1':True,
            './dir2/file2':True,
            './file3':True,
        }[path]

    @staticmethod
    def isdir(path):
        return {
            './': True,
            './dir1':True,
            './dir2':True,
            './dir1/file1':False,
            './dir2/file2':False,
            './file3':False,
        }[path]

    @staticmethod
    def islink(path):
        return {
            './': False,
            './dir1':False,
            './dir2':False,
            './dir1/file1':False,
            './dir2/file2':False,
            './file3':False,
        }[path]



    # @staticmethod
    # def mocked_walk(path):
    #     # FIXME return a generator instead
    #     return {
    #         './':('./',
    #             ['dir1', 'dir2', ],
    #             ['file3', ]),
    #         './dir1':('dir1',
    #             [],
    #             ['file1', ]),
    #         './dir2':('dir2',
    #             [],
    #             ['file2', ]),
    #     }[path]


    def setUp(self):
        # Mock this functions
        global isfile, islink, isdir #, walk

        self.old_isfile = isfile
        self.old_isdir = isdir
        self.old_islink = islink
        #self.old_walk = walk

        isfile = self.isfile
        isdir = self.isdir
        islink = self.islink
        #walk = self.mocked_walk


    def tearDown(self):
        global isfile, islink, isdir
        isfile = self.old_isfile
        isdir = self.old_isdir
        islink = self.old_islink
        #walk = self.old_walk


    def test_mocked_isfile(self):
        assert isfile('file3')

    def test_mocked_isfile_subdir(self):
        assert isfile('dir1/file1')

    def test_mocked_isfile_NOT(self):
        assert isfile('dir1') is False

    @raises(KeyError)
    def test_mocked_isfile_breaks(self):
        result = isfile('breaks')
        print 'isfile(\'breaks\'): %s' % result

    def test_mocked_isdir(self):
        assert isdir('dir1')

    def test_mocked_isdir_NOT(self):
        assert isdir('file3') == False

    @raises(KeyError)
    def test_mocked_isdir_breaks(self):
        result = isdir('breaks')

    def test_mocked_islink(self):
        assert islink('file3') == False

    @raises(KeyError)
    def test_mocked_islink_breaks(self):
        result = islink('breaks')
