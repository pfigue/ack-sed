#coding: utf-8

from setuptools import setup
from acksed.params import (project_name, project_version, project_description)


with open('README.rst', 'r') as f:
  long_description = f.read()


setup(name=project_name,
      version=project_version,
      description=project_description,
      long_description=long_description,
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
      ],
      keywords='sed command-line',
      url='https://bitbucket.org/pfigue/ack-sed',
      author='Pablo Figue',
      author_email='pablo.gfigue@gmail.com',
      license='MIT',
      packages=['acksed',],
      install_requires=['docopt',],  # FIXME read from requirements.txt
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'],
      scripts=['bin/ack-sed',],
)
