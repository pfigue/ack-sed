#!/usr/bin env python
#coding: utf-8


from nose.tools import (istest, raises, )

#
# SourceFileGenerator iteration habilities
#

from MockedOsPathFunctions import MockedOsPathFunctions
from MockedWalk import MockedWalkTest
from SourceFileGenerator import SourceFileGenerator


@istest
class SourceFileGeneratorTests(object):

    def setUp(self):
        global isfile, isdir, islink, walk

        self.old_isfile = main.isfile
        self.old_isdir = main.isdir
        self.old_islink = main.islink
        self.old_walk = main.walk

        main.isfile = MockedOsPathFunctions.isfile
        main.isdir = MockedOsPathFunctions.isdir
        main.islink = MockedOsPathFunctions.islink
        main.walk = MockedWalkTest.mocked_walk #FIXME rethink this

    def tearDown(self):
        global isfile, isdir, islink, walk

        main.isfile = self.old_isfile
        main.isdir = self.old_isdir
        main.islink = self.old_islink
        main.walk = self.old_walk

    def test_generator_ALLFiles_1st(self):
        arguments = {
            '--all': True,
            '--help': False,
            '--nopython': False,
            '--python': False,
            '--version': False,
            '<file>': ['./'],
            '<sed-expression>': 's/ABC/DEF/g'}
        obj = SourceFileGenerator(arguments)
        one_file = obj.next()
        assert one_file == './file3'

    def test_generator_ALLFiles_2st(self):
        arguments = {
            '--all': True,
            '--help': False,
            '--nopython': False,
            '--python': False,
            '--version': False,
            '<file>': ['./'],
            '<sed-expression>': 's/ABC/DEF/g'}
        obj = SourceFileGenerator(arguments)
        one_file = obj.next()
        one_file = obj.next()
        assert one_file == './dir1/file1'

    def test_generator_ALLFiles_3rd(self):
        arguments = {
            '--all': True,
            '--help': False,
            '--nopython': False,
            '--python': False,
            '--version': False,
            '<file>': ['./'],
            '<sed-expression>': 's/ABC/DEF/g'}
        obj = SourceFileGenerator(arguments)
        one_file = obj.next()
        one_file = obj.next()
        one_file = obj.next()
        assert one_file == './dir2/file2'

    def test_generator_ALLFiles_4th(self):
        arguments = {
            '--all': True,
            '--help': False,
            '--nopython': False,
            '--python': False,
            '--version': False,
            '<file>': ['./'],
            '<sed-expression>': 's/ABC/DEF/g'}
        obj = SourceFileGenerator(arguments)
        one_file = obj.next()
        one_file = obj.next()
        one_file = obj.next()
        one_file = obj.next()
        print one_file
        assert one_file == './dir2/file2' #FIXME this should be raising and StopIteration
