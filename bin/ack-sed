#!/usr/bin/env python
#coding: utf-8

from docopt import docopt
from acksed import (walk_and_filter_file_paths, sed_one_file)
from acksed.params import (project_name, project_version, project_description)


__doc__ = project_name + " " + project_version + """

Usage:
    ack-sed [options] <sed-expression> <file>...

Options:
    --all       Include all files, indepently of the extension.
    -h --help   Show this screen.
    --version   Show version.

    --actionscript    Apply expression only to Actionscript files.
    --noactionscript    Exclude Actionscript files.
    --ada    Apply expression only to Ada files.
    --noada    Exclude Ada files.
    --all    Apply expression only to All files.
    --noall    Exclude All files.
    --asp    Apply expression only to Asp files.
    --noasp    Exclude Asp files.
    --aspx    Apply expression only to Aspx files.
    --noaspx    Exclude Aspx files.
    --batch    Apply expression only to Batch files.
    --nobatch    Exclude Batch files.
    --cc    Apply expression only to Cc files.
    --nocc    Exclude Cc files.
    --cfmx    Apply expression only to Cfmx files.
    --nocfmx    Exclude Cfmx files.
    --clojure    Apply expression only to Clojure files.
    --noclojure    Exclude Clojure files.
    --cmake    Apply expression only to Cmake files.
    --nocmake    Exclude Cmake files.
    --coffeescript    Apply expression only to Coffeescript files.
    --nocoffeescript    Exclude Coffeescript files.
    --cpp    Apply expression only to Cpp files.
    --nocpp    Exclude Cpp files.
    --csharp    Apply expression only to Csharp files.
    --nocsharp    Exclude Csharp files.
    --css    Apply expression only to Css files.
    --nocss    Exclude Css files.
    --dart    Apply expression only to Dart files.
    --nodart    Exclude Dart files.
    --deplhi    Apply expression only to Deplhi files.
    --nodeplhi    Exclude Deplhi files.
    --elisp    Apply expression only to Elisp files.
    --noelisp    Exclude Elisp files.
    --elixir    Apply expression only to Elixir files.
    --noelixir    Exclude Elixir files.
    --erlang    Apply expression only to Erlang files.
    --noerlang    Exclude Erlang files.
    --fortran    Apply expression only to Fortran files.
    --nofortran    Exclude Fortran files.
    --go    Apply expression only to Go files.
    --nogo    Exclude Go files.
    --groovy    Apply expression only to Groovy files.
    --nogroovy    Exclude Groovy files.
    --haskell    Apply expression only to Haskell files.
    --nohaskell    Exclude Haskell files.
    --hh    Apply expression only to Hh files.
    --nohh    Exclude Hh files.
    --html    Apply expression only to Html files.
    --nohtml    Exclude Html files.
    --java    Apply expression only to Java files.
    --nojava    Exclude Java files.
    --js    Apply expression only to Js files.
    --nojs    Exclude Js files.
    --json    Apply expression only to Json files.
    --nojson    Exclude Json files.
    --jsp    Apply expression only to Jsp files.
    --nojsp    Exclude Jsp files.
    --less    Apply expression only to Less files.
    --noless    Exclude Less files.
    --lisp    Apply expression only to Lisp files.
    --nolisp    Exclude Lisp files.
    --lua    Apply expression only to Lua files.
    --nolua    Exclude Lua files.
    --make    Apply expression only to Make files.
    --nomake    Exclude Make files.
    --matlab    Apply expression only to Matlab files.
    --nomatlab    Exclude Matlab files.
    --objc    Apply expression only to Objc files.
    --noobjc    Exclude Objc files.
    --objcpp    Apply expression only to Objcpp files.
    --noobjcpp    Exclude Objcpp files.
    --ocaml    Apply expression only to Ocaml files.
    --noocaml    Exclude Ocaml files.
    --parrot    Apply expression only to Parrot files.
    --noparrot    Exclude Parrot files.
    --perl    Apply expression only to Perl files.
    --noperl    Exclude Perl files.
    --perltest    Apply expression only to Perltest files.
    --noperltest    Exclude Perltest files.
    --php    Apply expression only to Php files.
    --nophp    Exclude Php files.
    --plone    Apply expression only to Plone files.
    --noplone    Exclude Plone files.
    --python    Apply expression only to Python files.
    --nopython    Exclude Python files.
    --rake    Apply expression only to Rake files.
    --norake    Exclude Rake files.
    --rr    Apply expression only to Rr files.
    --norr    Exclude Rr files.
    --ruby    Apply expression only to Ruby files.
    --noruby    Exclude Ruby files.
    --rust    Apply expression only to Rust files.
    --norust    Exclude Rust files.
    --sass    Apply expression only to Sass files.
    --nosass    Exclude Sass files.
    --scala    Apply expression only to Scala files.
    --noscala    Exclude Scala files.
    --scheme    Apply expression only to Scheme files.
    --noscheme    Exclude Scheme files.
    --shell    Apply expression only to Shell files.
    --noshell    Exclude Shell files.
    --smalltalk    Apply expression only to Smalltalk files.
    --nosmalltalk    Exclude Smalltalk files.
    --sql    Apply expression only to Sql files.
    --nosql    Exclude Sql files.
    --tcl    Apply expression only to Tcl files.
    --notcl    Exclude Tcl files.
    --tex    Apply expression only to Tex files.
    --notex    Exclude Tex files.
    --tt    Apply expression only to Tt files.
    --nott    Exclude Tt files.
    --vb    Apply expression only to Vb files.
    --novb    Exclude Vb files.
    --verilog    Apply expression only to Verilog files.
    --noverilog    Exclude Verilog files.
    --vhdl    Apply expression only to Vhdl files.
    --novhdl    Exclude Vhdl files.
    --vim    Apply expression only to Vim files.
    --novim    Exclude Vim files.
    --xml    Apply expression only to Xml files.
    --noxml    Exclude Xml files.
    --yaml    Apply expression only to Yaml files.
    --noyaml    Exclude Yaml files.


"""
# FIXME docopt seems to approximate the parameters to the right one.
# E.g.: "--pytho" will be aproximated to "--python" if
# there is such "--python" option. I dislike this behaviour, as I think
# it will lead to errors in the user side.

# FIXME more extensions?


def main():
    arguments = docopt(__doc__, version='%s %s' % (project_name, project_version))
    sed_expression = arguments['<sed-expression>']
    #print 'sed_expression:', sed_expression
    source_files = walk_and_filter_file_paths(arguments)
    #print source_files
    #return 1 #FIXME remove

    for source_file in source_files:
        #print "source_file: %s" % source_file
        sed_one_file(sed_expression, source_file)
        # FIXME dry-run: print file names instead of sed()ing them


if __name__ == '__main__':
    main()
