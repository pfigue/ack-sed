#coding: utf-8

import re
from unittest import TestCase
from acksed import (walk_and_filter_file_paths, get_file_argument_as_list,
	prepare_include_and_exclude_lists, apply_filters_to_file)


class get_file_argument_as_listTest(TestCase):
	def test_if_when_lacking_file_parameter_will_return_local_dir(self):
	    arguments = {
	            '<file>': [],
	    }
	    result = get_file_argument_as_list(arguments)
	    print 'result = %s' % result
	    self.assertItemsEqual(result, ['./',])

	def test_if_accepts_a_list_with_the_local_dir(self):
	    arguments = {
	            '<file>': ['./'],
	    }
	    result = get_file_argument_as_list(arguments)
	    print 'result = %s' % result
	    self.assertItemsEqual(result, ['./',])

	def test_if_accepts_a_string_with_the_local_dir(self):
	    arguments = {
	            '<file>': './',
	    }
	    result = get_file_argument_as_list(arguments)
	    print 'result = %s' % result
	    self.assertItemsEqual(result, ['./',])


	def test_if_accepts_a_list_of_directories(self):
	    arguments = {
	            '<file>': ['foo/', 'bar/'],
	    }
	    result = get_file_argument_as_list(arguments)
	    print 'result = %s' % result
	    self.assertItemsEqual(result, ['foo/', 'bar/'])


class prepare_include_and_exclude_listsTest(TestCase):
	def test_to_include_only_python_files(self):
		arguments = {
			'--python': '',
		}
		(include, exclude) = prepare_include_and_exclude_lists(arguments)
		print 'Include:', include
		print 'Exclude:', exclude
		self.assertItemsEqual(include, [re.compile(r'.*\.py$'),])
		self.assertItemsEqual(exclude, [])

	def test_to_include_everything_but_python_files(self):
		arguments = {
			'--nopython': '',
		}
		(include, exclude) = prepare_include_and_exclude_lists(arguments)
		print 'Include:', include
		print 'Exclude:', exclude
		self.assertItemsEqual(include, [re.compile(r'.*$'), ])
		self.assertItemsEqual(exclude, [re.compile(r'.*\.py$'),])

	def test_to_include_only_python_and_yaml_files(self):
		arguments = {
			'--python': '',
			'--yaml': '',
		}
		(include, exclude) = prepare_include_and_exclude_lists(arguments)
		print 'Include:', include
		print 'Exclude:', exclude
		self.assertItemsEqual(include, [re.compile(r'.*\.py$'), re.compile(r'.*\.(yml|yaml)$'),])
		self.assertItemsEqual(exclude, [])

	def test_to_include_neither_python_nor_yaml_files(self):
		arguments = {
			'--nopython': '',
			'--noyaml': '',
		}
		(include, exclude) = prepare_include_and_exclude_lists(arguments)
		print 'Include:', include
		print 'Exclude:', exclude
		self.assertItemsEqual(include, [re.compile(r'.*$'), ])
		self.assertItemsEqual(exclude, [re.compile(r'.*\.py$'), re.compile(r'.*\.(yml|yaml)$'),])

	def test_to_include_no_python_but_yaml_files(self):
		arguments = {
			'--nopython': '',
			'--yaml': '',
		}
		(include, exclude) = prepare_include_and_exclude_lists(arguments)
		print 'Include:', include
		print 'Exclude:', exclude
		self.assertItemsEqual(include, [re.compile(r'.*\.(yml|yaml)$'), ])
		self.assertItemsEqual(exclude, [re.compile(r'.*\.py$'), ])


class apply_filters_to_fileTest(TestCase):
	def test_if_py_and_yaml_files_are_accepted_when_py_and_yaml_are_requested(self):
		include = [
			re.compile(r'.*\.py$'),
			re.compile(r'.*\.(yml|yaml)$'),]
		exclude = []
		(have_to_include, have_to_exclude) = apply_filters_to_file('source.py', include, exclude)
		self.assertTrue(have_to_include)
		self.assertFalse(have_to_exclude)
		(have_to_include, have_to_exclude) = apply_filters_to_file('data.yml', include, exclude)
		self.assertTrue(have_to_include)
		self.assertFalse(have_to_exclude)

	def test_if_py_file_is_accepted_but_yaml_is_not(self):
		include = [
			re.compile(r'.*\.(yml|yaml)$'), ]
		exclude = [
			re.compile(r'.*\.py$'), ]
		(have_to_include, have_to_exclude) = apply_filters_to_file('data.yml', include, exclude)		
		self.assertTrue(have_to_include)
		self.assertFalse(have_to_exclude)
		(have_to_include, have_to_exclude) = apply_filters_to_file('source.py', include, exclude)
		self.assertFalse(have_to_include)
		self.assertTrue(have_to_exclude)

	def test_if_neither_py_file_nor_yaml_are_accepted(self):
		include = []
		exclude = [
			re.compile(r'.*\.py$'),
			re.compile(r'.*\.(yml|yaml)$'),]
		(have_to_include, have_to_exclude) = apply_filters_to_file('data.yml', include, exclude)		
		self.assertFalse(have_to_include)
		self.assertTrue(have_to_exclude)
		(have_to_include, have_to_exclude) = apply_filters_to_file('source.py', include, exclude)
		self.assertFalse(have_to_include)
		self.assertTrue(have_to_exclude)

	def test_if_all_files_are_accepted(self):
		include = [
			re.compile(r'.*$'), ]
		exclude = []
		(have_to_include, have_to_exclude) = apply_filters_to_file('data.yml', include, exclude)		
		self.assertTrue(have_to_include)
		self.assertFalse(have_to_exclude)
		(have_to_include, have_to_exclude) = apply_filters_to_file('source.py', include, exclude)
		self.assertTrue(have_to_include)
		self.assertFalse(have_to_exclude)


class walk_and_filter_file_pathsTest(TestCase):
	def test_if_you_find_3_files_in_a_dir(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_files/'
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		self.assertItemsEqual(file_list,
			['./acksed/tests/a_dir_with_3_files/file3',
			'./acksed/tests/a_dir_with_3_files/file2',
			'./acksed/tests/a_dir_with_3_files/file1'])

	def test_if_you_find_3_files_in_a_dir_and_a_subdir(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_files_and_a_subdir/'
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list,
			['./acksed/tests/a_dir_with_3_files_and_a_subdir/subdir/file3',
			'./acksed/tests/a_dir_with_3_files_and_a_subdir/file2',
			'./acksed/tests/a_dir_with_3_files_and_a_subdir/file1'])

	def test_if_you_find_3_files_in_a_dir_and_a_subdir_and_avoid_duplicates(self):
		arguments = {
			'<file>': [
				'./acksed/tests/a_dir_with_3_files_and_a_subdir/',
				'./acksed/tests/a_dir_with_3_files_and_a_subdir/subdir/',
				]
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list,
			['./acksed/tests/a_dir_with_3_files_and_a_subdir/subdir/file3',
			'./acksed/tests/a_dir_with_3_files_and_a_subdir/file2',
			'./acksed/tests/a_dir_with_3_files_and_a_subdir/file1'])

	def test_if_can_filter_only_python_files(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_yml_and_2_py_files/',
			'--python': '',
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list,
			[
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file1.py',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file2.py',
			])

	def test_if_can_filter_all_but_python_files(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_yml_and_2_py_files/',
			'--nopython': '',
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list,
			[
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file3.yml',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file4.yml',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file5.yml',
			])

	def test_if_can_filter_all_but_python_and_yaml_files(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_yml_and_2_py_files/',
			'--nopython': '',
			'--noyaml': '',
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list, [])

	def test_if_can_filter_python_and_yaml_files(self):
		arguments = {
			'<file>': './acksed/tests/a_dir_with_3_yml_and_2_py_files/',
			'--python': '',
			'--yaml': '',
		}
		#print arguments
		file_list = walk_and_filter_file_paths(arguments)
		print file_list
		self.assertItemsEqual(file_list, 
			[
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file1.py',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file2.py',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file3.yml',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file4.yml',
				'./acksed/tests/a_dir_with_3_yml_and_2_py_files/file5.yml',
			])
